
(function($) {

  $("#contact").submit(function(e){
    e.preventDefault();
    if ( $( "#demo-human" ).val() ) {
      axios({
        method: 'post',
        url: 'https://api.cead.ai/contact',
        data: {
          name: $( "#demo-name" ).val(),
          email: $( "#demo-email" ).val(),
          category: $( "select#demo-category option:checked" ).val(),
          priority: $( "input[type=radio][name=demo-priority]:checked" ).val(),
          message: $( "#demo-message" ).val()
        }
      })
        .then(response => {
          console.log(response);
        })
        .catch(err => {
          console.log(err);
        })
    } else {
      console.log('nice try robots');
    }
  });

})(jQuery);
