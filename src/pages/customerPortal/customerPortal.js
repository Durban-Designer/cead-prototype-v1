import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../actions/navigationController';
import BG from '../../assets/images/screensv6/customerPortal.png';

function mapStateToProps(state) {
  return {
    // for when customer redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class CustomerPortal extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('login')}} className={classes.login}></div>
        <div onClick={() => {this.props.handleNavigation('customerReports')}} className={classes.customerReports}></div>
        <div onClick={() => {this.props.handleNavigation('customerLocationsView')}} className={classes.customerLocations}></div>
        <div onClick={() => {this.props.handleNavigation('customerReports')}} className={classes.customerReportsNav}></div>
        <div onClick={() => {this.props.handleNavigation('customerLocationsView')}} className={classes.customerLocationsNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  login: {
    position: 'absolute',
    left: '130px',
    top: '0vh',
    width: '1900px',
    height: '125px',
    background: 'none',
    zIndex: 2
  },
  customerReports: {
    position: 'absolute',
    left: '260px',
    top: '210px',
    width: '420px',
    height: '230px',
    background: 'none',
    zIndex: 2
  },
  customerLocations: {
    position: 'absolute',
    left: '260px',
    top: '445px',
    width: '420px',
    height: '230px',
    background: 'none',
    zIndex: 2
  },
  customerReportsNav: {
    position: 'absolute',
    left: '0vw',
    top: '125px',
    width: '130px',
    height: '150px',
    background: 'none',
    zIndex: 2
  },
  customerLocationsNav: {
    position: 'absolute',
    left: '0vw',
    top: '260px',
    width: '130px',
    height: '150px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CustomerPortal));
