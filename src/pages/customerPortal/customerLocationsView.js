import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../actions/navigationController';
import BG from '../../assets/images/screensv6/customerLocations.png';

function mapStateToProps(state) {
  return {
    // for when customer redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class CustomerLocationsView extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('customerPortal')}} className={classes.customerPortal}></div>
        <div onClick={() => {this.props.handleNavigation('customerReports')}} className={classes.customerReportsNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  customerPortal: {
    position: 'absolute',
    left: '130px',
    top: '0vh',
    width: '1900px',
    height: '125px',
    background: 'none',
    zIndex: 2
  },
  customerReportsNav: {
    position: 'absolute',
    left: '0vw',
    top: '125px',
    width: '130px',
    height: '150px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CustomerLocationsView));
