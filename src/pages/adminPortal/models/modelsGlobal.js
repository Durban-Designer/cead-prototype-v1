import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../../actions/navigationController';
import BG from '../../../assets/images/screensv6/modelsListview.png';

function mapStateToProps(state) {
  return {
    // for when sensors redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class ModelsGlobal extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('modelsDetail')}} className={classes.modelsDetail}></div>
        <div onClick={() => {this.props.handleNavigation('companyDetail')}} className={classes.companyDetailNav}></div>
        <div onClick={() => {this.props.handleNavigation('locationsGlobal')}} className={classes.locationsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('reportsGlobal')}} className={classes.reportsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('admin')}} className={classes.adminNav}></div>
        <div onClick={() => {this.props.handleNavigation('dataFlowsGlobal')}} className={classes.dataFlowsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('modelsGlobal')}} className={classes.modelsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('actionsGlobal')}} className={classes.actionsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('sensorsGlobal')}} className={classes.sensorsGlobalSubNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  modelsDetail: {
    position: 'absolute',
    left: '260px',
    top: '235px',
    width: '1290px',
    height: '800px',
    background: 'none',
    zIndex: 2
  },
  companyDetailNav: {
    position: 'absolute',
    left: '0vw',
    top: '250px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  locationsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '385px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  reportsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '500px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  adminNav: {
    position: 'absolute',
    left: '0vw',
    top: '640px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  dataFlowsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '760px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  modelsGlobalSubNav: {
    position: 'absolute',
    left: '135px',
    top: '135px',
    width: '120px',
    height: '120px',
    background: 'none',
    zIndex: 2
  },
  actionsGlobalSubNav: {
    position: 'absolute',
    left: '135px',
    top: '260px',
    width: '120px',
    height: '120px',
    background: 'none',
    zIndex: 2
  },
  sensorsGlobalSubNav: {
    position: 'absolute',
    left: '135px',
    top: '400px',
    width: '120px',
    height: '120px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ModelsGlobal));
