import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import img from '../../../assets/images/Icons/eye.svg';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { mdiAccount } from '@mdi/js';
import { handleNavigation } from '../../../actions/navigationController';
import { blueprintSwitch } from '../../../actions/blueprintController';
import BG from '../../../assets/images/screensv6/blueprintEditview.png';

const drawerWidth = 85;

function mapStateToProps(state) {
  return {
    // for when sensors redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class Blueprint extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('login')}} className={classes.login}></div>
        <div onClick={() => {this.props.handleNavigation('companyDetail')}} className={classes.companyDetailNav}></div>
        <div onClick={() => {this.props.handleNavigation('locationsGlobal')}} className={classes.locationsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('reportsGlobal')}} className={classes.reportsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('admin')}} className={classes.adminNav}></div>
        <div onClick={() => {this.props.handleNavigation('dataFlowsGlobal')}} className={classes.dataFlowsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('modelsGlobal')}} className={classes.modelsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('actionsGlobal')}} className={classes.actionsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('sensorsGlobal')}} className={classes.sensorsGlobalSubNav}></div>
      </div>
    );
  }
}

const styles = theme => ({
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  companyDetailNav: {
    position: 'absolute',
    left: '0vw',
    top: '250px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  locationsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '385px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  reportsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '500px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  adminNav: {
    position: 'absolute',
    left: '0vw',
    top: '640px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  dataFlowsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '760px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Blueprint));
