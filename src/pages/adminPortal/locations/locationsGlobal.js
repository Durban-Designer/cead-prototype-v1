import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../../actions/navigationController';
import BG from '../../../assets/images/screensv6/locations.png';

function mapStateToProps(state) {
  return {
    // for when sensors redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class LocationsGlobal extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('blueprint')}} className={classes.blueprint}></div>
        <div onClick={() => {this.props.handleNavigation('companyGlobal')}} className={classes.companyGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('companyDetail')}} className={classes.companyDetailNav}></div>
        <div onClick={() => {this.props.handleNavigation('locationsGlobal')}} className={classes.locationsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('reportsGlobal')}} className={classes.reportsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('admin')}} className={classes.adminNav}></div>
        <div onClick={() => {this.props.handleNavigation('dataFlowsGlobal')}} className={classes.dataFlowsGlobalNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  blueprint: {
    position: 'absolute',
    left: '260px',
    top: '235px',
    width: '1490px',
    height: '800px',
    background: 'none',
    zIndex: 2
  },
  companyDetailNav: {
    position: 'absolute',
    left: '0vw',
    top: '250px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  locationsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '385px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  reportsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '500px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  adminNav: {
    position: 'absolute',
    left: '0vw',
    top: '640px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  },
  dataFlowsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '760px',
    width: '130px',
    height: '135px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LocationsGlobal));
