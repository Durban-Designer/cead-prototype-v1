import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../../actions/navigationController';

function mapStateToProps(state) {
  return {
    // for when sensors redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class DataFlowVisualizer extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DataFlowVisualizer));
