import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../../actions/navigationController';
import BG from '../../../assets/images/screensv6/hardwareTracking.png';

function mapStateToProps(state) {
  return {
    // for when hardware redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class HardwareTrackingPortal extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('adminPortal')}} className={classes.adminPortal}></div>
        <div onClick={() => {this.props.handleNavigation('hardwareTestPortal')}} className={classes.hardwareTestPortal}></div>
        <div onClick={() => {this.props.handleNavigation('hardwareInventory')}} className={classes.hardwareInventory}></div>
        <div onClick={() => {this.props.handleNavigation('companyGlobal')}} className={classes.companyGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('companyDetail')}} className={classes.companyDetailNav}></div>
        <div onClick={() => {this.props.handleNavigation('locationsGlobal')}} className={classes.locationsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('reportsGlobal')}} className={classes.reportsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('admin')}} className={classes.adminNav}></div>
        <div onClick={() => {this.props.handleNavigation('dataFlowsGlobal')}} className={classes.dataFlowsGlobalNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  adminPortal: {
    position: 'absolute',
    left: '5vw',
    top: '0vh',
    width: '1200px',
    height: '80px',
    background: 'none',
    zIndex: 2
  },
  hardwareTestPortal: {
    position: 'absolute',
    left: '13vw',
    top: '20vh',
    width: '200px',
    height: '400px',
    background: 'none',
    zIndex: 2
  },
  hardwareInventory: {
    position: 'absolute',
    left: '39vw',
    top: '20vh',
    width: '200px',
    height: '400px',
    background: 'none',
    zIndex: 2
  },
  companyGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '12vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  companyDetailNav: {
    position: 'absolute',
    left: '0vw',
    top: '24vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  locationsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '36vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  reportsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '48vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  adminNav: {
    position: 'absolute',
    left: '0vw',
    top: '60vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  dataFlowsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '72vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HardwareTrackingPortal));
