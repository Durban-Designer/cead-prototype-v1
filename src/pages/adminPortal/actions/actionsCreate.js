import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from '../../../actions/navigationController';
import BG from '../../../assets/images/screens/actionsCreate.png';

function mapStateToProps(state) {
  return {
    // for when sensors redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class ActionsCreate extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <div onClick={() => {this.props.handleNavigation('companyGlobal')}} className={classes.companyGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('companyDetail')}} className={classes.companyDetailNav}></div>
        <div onClick={() => {this.props.handleNavigation('locationsGlobal')}} className={classes.locationsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('reportsGlobal')}} className={classes.reportsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('admin')}} className={classes.adminNav}></div>
        <div onClick={() => {this.props.handleNavigation('dataFlowsGlobal')}} className={classes.dataFlowsGlobalNav}></div>
        <div onClick={() => {this.props.handleNavigation('modelsGlobal')}} className={classes.modelsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('actionsGlobal')}} className={classes.actionsGlobalSubNav}></div>
        <div onClick={() => {this.props.handleNavigation('sensorsGlobal')}} className={classes.sensorsGlobalSubNav}></div>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  companyGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '12vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  companyDetailNav: {
    position: 'absolute',
    left: '0vw',
    top: '24vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  locationsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '36vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  reportsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '48vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  adminNav: {
    position: 'absolute',
    left: '0vw',
    top: '60vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  dataFlowsGlobalNav: {
    position: 'absolute',
    left: '0vw',
    top: '72vh',
    width: '50px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  modelsGlobalSubNav: {
    position: 'absolute',
    left: '5vw',
    top: '12vh',
    width: '60px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  actionsGlobalSubNav: {
    position: 'absolute',
    left: '5vw',
    top: '24vh',
    width: '60px',
    height: '70px',
    background: 'none',
    zIndex: 2
  },
  sensorsGlobalSubNav: {
    position: 'absolute',
    left: '5vw',
    top: '36vh',
    width: '60px',
    height: '70px',
    background: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ActionsCreate));
