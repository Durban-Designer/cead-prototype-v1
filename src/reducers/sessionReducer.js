const initialState = {
  userId: '',
  token: '',
  vertical: '',
  admin: ''
}
export default function(state = initialState, action) {
  switch (action.type) {
    case "updateUser":
      return {
        ...state,
        userId: action.payload.data.userId,
        token: action.payload.data.token,
        vertical: action.payload.data.vertical,
        admin: action.payload.data.admin
      };
    case "logout":
      return {
        ...state,
        userId: '',
        token: '',
        vertical: '',
        admin: ''
      }
    default:
      return state;
  }
}
