import { combineReducers } from 'redux';
import navigationReducer from './navigationReducer';
import sessionReducer from './sessionReducer';

export default combineReducers({
 navigationReducer,
 sessionReducer,
});
