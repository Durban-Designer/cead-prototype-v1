import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from './actions/navigationController';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

function mapStateToProps(state) {
  return {
    // for if navbar redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route))
  };
}

class Navbar extends Component {
  constructor (props) {
    super(props);
    this.state = {
      left: false
    };
    this.togglePane = this.togglePane.bind(this);
  }

  togglePane () {
    var bool = !this.state.left
    this.setState({
      left: bool
    })
  }

  render(){
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <button
          className={classes.invisible}
          onClick={() => this.setState({ left: !this.state.left })}
        >
        </button>

        <Drawer open={this.state.left} onClose={this.togglePane} className={classes.drawer}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.togglePane}
          >
            <List className={classes.list}>
              {['login', 'customerPortal', 'customerTicket', 'customerLocationsView', 'customerReports',
                'adminPortal', 'hardwareTrackingPortal', 'hardwareTestPortal', 'hardwareInventory', 'companyGlobal',
                'companyDetail', 'admin', 'adminTickets', 'adminUsersPanel', 'adminRolesPanel', 'adminCompaniesPanel',
                'sensorsGlobal', 'sensorsDetail', 'modelsGlobal', 'modelsCreate', 'modelsTraining', 'modelsDeployment',
                'modelsDetail', 'actionsGlobal', 'actionsCreate', 'actionsDetail', 'reportsGlobal', 'reportsCreate',
                'reportsDetail', 'locationsGlobal', 'blueprint', 'dataFlowsGlobal', 'dataFlowVisualizer'
              ].map((text, index) => (
                <ListItem button key={text} onClick={() => {
                    this.props.handleNavigation(text);
                  }}>
                  <ListItemText primary={text} />
                </ListItem>
              ))}
            </List>
          </div>
        </Drawer>
      </div>
    );
  }
}

const styles = {
  drawer: {
    width: '26vw',
    overflow: 'hidden',
    flexShrink: 0,
    zIndex: 4,
  },
  list: {
    marginTop: '26vh',
    marginLeft: '4vw',
    width: '26vw',
  },
  grow: {
    flexGrow: 1,
  },
  invisible: {
    position: 'absolute',
    left: '10vw',
    top: '10vh',
    width: '100px',
    height: '100px',
    background: 'none',
    border: 'none',
    zIndex: 2
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Navbar));
