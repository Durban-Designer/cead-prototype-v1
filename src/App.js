import React, { Component } from 'react';
import Navbar from './navbar';
import { Redirect, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import { resetNavigation } from './actions/navigationController';
import login from './login.js';
import customerPortal from './pages/customerPortal/customerPortal.js';
import customerTicket from './pages/customerPortal/customerTicket.js';
import customerLocationsView from './pages/customerPortal/customerLocationsView.js';
import customerReports from './pages/customerPortal/customerReports.js';
import adminPortal from './pages/adminPortal/adminPortal.js';
import hardwareTrackingPortal from './pages/adminPortal/hardwareTracking/hardwareTrackingPortal.js';
import hardwareTestPortal from './pages/adminPortal/hardwareTracking/hardwareTestPortal.js';
import hardwareInventory from './pages/adminPortal/hardwareTracking/hardwareInventory.js';
import companyGlobal from './pages/adminPortal/companyViews/companyGlobal.js';
import companyDetail from './pages/adminPortal/companyViews/companyDetail.js';
import admin from './pages/adminPortal/admin/admin.js';
import adminTickets from './pages/adminPortal/admin/adminTickets.js';
import adminUsersPanel from './pages/adminPortal/admin/adminUsersPanel.js';
import adminRolesPanel from './pages/adminPortal/admin/adminRolesPanel.js';
import adminCompaniesPanel from './pages/adminPortal/admin/adminCompaniesPanel.js';
import sensorsGlobal from './pages/adminPortal/sensors/sensorsGlobal.js';
import sensorsDetail from './pages/adminPortal/sensors/sensorsDetail.js';
import modelsGlobal from './pages/adminPortal/models/modelsGlobal.js';
import modelsCreate from './pages/adminPortal/models/modelsCreate.js';
import modelsTraining from './pages/adminPortal/models/modelsTraining.js';
import modelsDeployment from './pages/adminPortal/models/modelsDeployment.js';
import modelsDetail from './pages/adminPortal/models/modelsDetail.js';
import actionsGlobal from './pages/adminPortal/actions/actionsGlobal.js';
import actionsCreate from './pages/adminPortal/actions/actionsCreate.js';
import actionsDetail from './pages/adminPortal/actions/actionsDetail.js';
import reportsGlobal from './pages/adminPortal/reports/reportsGlobal.js';
import reportsCreate from './pages/adminPortal/reports/reportsCreate.js';
import reportsDetail from './pages/adminPortal/reports/reportsDetail.js';
import locationsGlobal from './pages/adminPortal/locations/locationsGlobal.js';
import blueprint from './pages/adminPortal/locations/blueprint.js';
import dataFlowsGlobal from './pages/adminPortal/dataFlows/dataFlowsGlobal.js';
import dataFlowVisualizer from './pages/adminPortal/dataFlows/dataFlowVisualizer.js';
import NoMatch from './404.js';

function mapStateToProps(state) {
  return {
    toLogin: state.navigationReducer.toLogin,
    toCustomerPortal: state.navigationReducer.toCustomerPortal,
    toCustomerTicket: state.navigationReducer.toCustomerTicket,
    toCustomerLocationsView: state.navigationReducer.toCustomerLocationsView,
    toCustomerReports: state.navigationReducer.toCustomerReports,
    toAdminPortal: state.navigationReducer.toAdminPortal,
    toHardwareTrackingPortal: state.navigationReducer.toHardwareTrackingPortal,
    toHardwareTestPortal: state.navigationReducer.toHardwareTestPortal,
    toHardwareInventory: state.navigationReducer.toHardwareInventory,
    toCompanyGlobal: state.navigationReducer.toCompanyGlobal,
    toCompanyDetail: state.navigationReducer.toCompanyDetail,
    toAdmin: state.navigationReducer.toAdmin,
    toAdminTickets: state.navigationReducer.toAdminTickets,
    toAdminUsersPanel: state.navigationReducer.toAdminUsersPanel,
    toAdminRolesPanel: state.navigationReducer.toAdminRolesPanel,
    toAdminCompaniesPanel: state.navigationReducer.toAdminCompaniesPanel,
    toSensorsGlobal: state.navigationReducer.toSensorsGlobal,
    toSensorsDetail: state.navigationReducer.toSensorsDetail,
    toModelsGlobal: state.navigationReducer.toModelsGlobal,
    toModelsCreate: state.navigationReducer.toModelsCreate,
    toModelsTraining: state.navigationReducer.toModelsTraining,
    toModelsDeployment: state.navigationReducer.toModelsDeployment,
    toModelsDetail: state.navigationReducer.toModelsDetail,
    toActionsGlobal: state.navigationReducer.toActionsGlobal,
    toActionsCreate: state.navigationReducer.toActionsCreate,
    toActionsDetail: state.navigationReducer.toActionsDetail,
    toReportsGlobal: state.navigationReducer.toReportsGlobal,
    toReportsCreate: state.navigationReducer.toReportsCreate,
    toReportsDetail: state.navigationReducer.toReportsDetail,
    toLocationsGlobal: state.navigationReducer.toLocationsGlobal,
    toBlueprint: state.navigationReducer.toBlueprint,
    toDataFlowsGlobal: state.navigationReducer.toDataFlowsGlobal,
    toDataFlowVisualizer: state.navigationReducer.toDataFlowVisualizer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    resetNavigation: () => dispatch(resetNavigation())
  }
}

class App extends Component {
  componentWillReceiveProps(props) {
    setTimeout(100, this.props.resetNavigation())
  }
  render() {
    var url
    if (this.props.toLogin) {
      url = <Redirect to='/' />
    } else if (this.props.toCustomerPortal) {
      url = <Redirect to='/customerPortal' />
    } else if (this.props.toCustomerTicket) {
      url = <Redirect to='/customerTicket' />
    } else if (this.props.toCustomerLocationsView) {
      url = <Redirect to='/customerLocationsView' />
    } else if (this.props.toCustomerReports) {
      url = <Redirect to='/customerReports' />
    } else if (this.props.toAdminPortal) {
      url = <Redirect to='/adminPortal' />
    } else if (this.props.toHardwareTrackingPortal) {
      url = <Redirect to='/hardwareTrackingPortal' />
    } else if (this.props.toHardwareTestPortal) {
      url = <Redirect to='/hardwareTestPortal' />
    } else if (this.props.toHardwareInventory) {
      url = <Redirect to='/hardwareInventory' />
    } else if (this.props.toCompanyGlobal) {
      url = <Redirect to='/companyGlobal' />
    } else if (this.props.toCompanyDetail) {
      url = <Redirect to='/companyDetail' />
    } else if (this.props.toAdmin) {
      url = <Redirect to='/admin' />
    } else if (this.props.toAdminTickets) {
      url = <Redirect to='/adminTickets' />
    } else if (this.props.toAdminUsersPanel) {
      url = <Redirect to='/adminUsersPanel' />
    } else if (this.props.toAdminRolesPanel) {
      url = <Redirect to='/adminRolesPanel' />
    } else if (this.props.toAdminCompaniesPanel) {
      url = <Redirect to='/adminCompaniesPanel' />
    } else if (this.props.toSensorsGlobal) {
      url = <Redirect to='/sensorsGlobal' />
    } else if (this.props.toSensorsDetail) {
      url = <Redirect to='/sensorsDetail' />
    } else if (this.props.toModelsGlobal) {
      url = <Redirect to='/modelsGlobal' />
    } else if (this.props.toModelsCreate) {
      url = <Redirect to='/modelsCreate' />
    } else if (this.props.toModelsTraining) {
      url = <Redirect to='/modelsTraining' />
    } else if (this.props.toModelsDeployment) {
      url = <Redirect to='/modelsDeployment' />
    } else if (this.props.toModelsDetail) {
      url = <Redirect to='/modelsDetail' />
    } else if (this.props.toActionsGlobal) {
      url = <Redirect to='/actionsGlobal' />
    } else if (this.props.toActionsCreate) {
      url = <Redirect to='/actionsCreate' />
    } else if (this.props.toActionsDetail) {
      url = <Redirect to='/actionsDetail' />
    } else if (this.props.toReportsGlobal) {
      url = <Redirect to='/reportsGlobal' />
    } else if (this.props.toReportsCreate) {
      url = <Redirect to='/reportsCreate' />
    } else if (this.props.toReportsDetail) {
      url = <Redirect to='/reportsDetail' />
    } else if (this.props.toLocationsGlobal) {
      url = <Redirect to='/locationsGlobal' />
    } else if (this.props.toBlueprint) {
      url = <Redirect to='/blueprint' />
    } else if (this.props.toDataFlowsGlobal) {
      url = <Redirect to='/dataFlowsGlobal' />
    } else if (this.props.toDataFlowVisualizer) {
      url = <Redirect to='/dataFlowVisualizer' />
    } else {
      url = <div></div>
    }
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <BrowserRouter>
            <div>
              <Navbar />
              {url}
              <Switch>
                <Route exact path="/" component={login}/>
                <Route path="/customerPortal" component={customerPortal}/>
                <Route path="/customerTicket" component={customerTicket}/>
                <Route path="/customerLocationsView" component={customerLocationsView}/>
                <Route path="/customerReports" component={customerReports}/>
                <Route path="/adminPortal" component={adminPortal}/>
                <Route path="/hardwareTrackingPortal" component={hardwareTrackingPortal}/>
                <Route path="/hardwareTestPortal" component={hardwareTestPortal}/>
                <Route path="/hardwareInventory" component={hardwareInventory}/>
                <Route path="/companyGlobal" component={companyGlobal}/>
                <Route path="/companyDetail" component={companyDetail}/>
                <Route path="/admin" component={admin}/>
                <Route path="/adminTickets" component={adminTickets}/>
                <Route path="/adminUsersPanel" component={adminUsersPanel}/>
                <Route path="/adminRolesPanel" component={adminRolesPanel}/>
                <Route path="/adminCompaniesPanel" component={adminCompaniesPanel}/>
                <Route path="/sensorsGlobal" component={sensorsGlobal}/>
                <Route path="/sensorsDetail" component={sensorsDetail}/>
                <Route path="/modelsGlobal" component={modelsGlobal}/>
                <Route path="/modelsCreate" component={modelsCreate}/>
                <Route path="/modelsTraining" component={modelsTraining}/>
                <Route path="/modelsDeployment" component={modelsDeployment}/>
                <Route path="/modelsDetail" component={modelsDetail}/>
                <Route path="/actionsGlobal" component={actionsGlobal}/>
                <Route path="/actionsCreate" component={actionsCreate}/>
                <Route path="/actionsDetail" component={actionsDetail}/>
                <Route path="/reportsGlobal" component={reportsGlobal}/>
                <Route path="/reportsCreate" component={reportsCreate}/>
                <Route path="/reportsDetail" component={reportsDetail}/>
                <Route path="/locationsGlobal" component={locationsGlobal}/>
                <Route path="/blueprint" component={blueprint}/>
                <Route path="/dataFlowsGlobal" component={dataFlowsGlobal}/>
                <Route path="/dataFlowVisualizer" component={dataFlowVisualizer}/>
                <Route component={NoMatch}/>
              </Switch>
            </div>
          </BrowserRouter>
        </div>
      </MuiThemeProvider>
    );
  }
}

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: '"Montserrat", sans-serif',
    color: 'primary',
  },
  overrides: {
    MuiSelect: {
      select: {
        textAlign: 'left',
        '&:focus': {
          backgroundColor: '#fff',
        }
      },
    },
    MuiTooltip: {
      tooltipPlacementLeft: {
        backgroundColor: '#fff',
        border: '1px solid #ff0000',
        color: '#000',
        fontWeight: 'bold',
      },
    },
    MuiDrawer: {
      paper: {
        overflowX: 'scroll',
        overflowY: 'visible'
      },
    },
    MuiButton: {
      root: {
        fontFamily: '"Montserrat", sans-serif',
      },
    },
    MuiStepLabel: {
      label: {
        color: '#fff !important',
      }
    },
    MuiStepIcon: {
      root: {
        color: '#b0b0b0',
        width: 'auto',
        height: '4vh',
        zIndex: 3,
      },
      active: {
        color: '#ff0000 !important',
        zIndex: 3,
      },
      completed: {
        color: '#ff0000 !important',
        zIndex: 3,
      },
      text: {
        color: 'white',
        fill: 'inherit'
      }
    },
    MuiDivider: {
      root: {
        position: 'absolute',
        left: 0,
        height: '2px',
        width: '100vw',
      },
    },
  },
  palette: {
    primary: { main: '#ffffff' },
    secondary: { main: '#252525' },
    tertiary: { main: '#ff0000' },
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
