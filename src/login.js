import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { handleNavigation } from './actions/navigationController';
import { updateUser } from './actions/sessionController';
// material UI components
import Typography from '@material-ui/core/Typography';
import Icon from "@material-ui/core/Icon";
import Button from '@material-ui/core/Button';
import Card from './components/Card/Card';
import Email from "@material-ui/icons/Email";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Grid from '@material-ui/core/Grid';
import GridContainer from "./components/Grid/GridContainer.jsx";
import GridItem from "./components/Grid/GridItem.jsx";
import TextField from '@material-ui/core/TextField';
import BG from './assets/images/screens/adminLogin.png';

function mapStateToProps(state) {
  return {
    // for when login redux is added
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleNavigation: route => dispatch(handleNavigation(route)),
    updateUser: user => dispatch(updateUser(user))
  };
}

class Login extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      showPass: false
    }
    this.login = this.login.bind(this);
    this.updateEmail = this.updateEmail.bind(this);
    this.updatePassword = this.updatePassword.bind(this);
  }

  login () {
    axios.post('https://api.cead.ai/users/login', {
      email: this.state.email,
      password: this.state.password
    })
      .then(response => {
        this.props.updateUser(response);
        this.props.handleNavigation('companyDetail');
        // todo add send to either company or customer portals
      })
      .catch(err => {
        console.log(err);
      })
  }

  updateEmail (evt) {
    this.setState({ email: evt.target.value });
  }

  updatePassword (evt) {
    this.setState({ password: evt.target.value });
  }

  render () {
    const { classes } = this.props;
    var pass

    if (this.state.showPass) {
      pass = (
        <Grid container spacing={16} alignItems="flex-end">
          <Grid item xs={2}>
            <Icon className={classes.inputIconsColor}>
              lock-outline
            </Icon>
          </Grid>
          <Grid item xs={9}>
            <TextField
              label="* Password"
              id="password"
              className={classes.input}
              value={this.state.password}
              onChange={this.updatePassword}
              fullWidth={true}
              margin="dense"
              type='text'
            />
          </Grid>
          <Grid item xs={1}>
            <VisibilityOff className={classes.inputIconsColor} onClick={() => this.setState({ showPass: false })}/>
          </Grid>
        </Grid>
      );
    } else {
      pass = (
        <Grid container spacing={16} alignItems="flex-end">
          <Grid item xs={2}>
            <Icon className={classes.inputIconsColor}>
              lock-outline
            </Icon>
          </Grid>
          <Grid item xs={9}>
            <TextField
              label="* Password"
              id="password"
              className={classes.input}
              value={this.state.password}
              onChange={this.updatePassword}
              fullWidth={true}
              margin="dense"
              type='password'
            />
          </Grid>
          <Grid item xs={1}>
            <Visibility className={classes.inputIconsColor} onClick={() => this.setState({ showPass: true })}/>
          </Grid>
        </Grid>
      );
    }
    return (
      <div className={classes.main}>
        <div className={classes.background}></div>
        <Card className={classes.card}>
          <GridContainer justify='center' className={classes.formContainer}>
            <GridItem xs={10} className={classes.gridItem}>
              <Typography variant='h4' className={classes.header}>
                Platform Login
              </Typography>
            </GridItem>
            <GridItem xs={10} className={classes.gridItem}>
              <Typography variant='body1' className={classes.header2}>
                Spearheading the fourth industrial revolution
              </Typography>
            </GridItem>
            <GridItem xs={10} className={classes.gridItem}>
              <Grid container spacing={16} alignItems="flex-end">
                <Grid item xs={2}>
                  <Email className={classes.inputIconsColor} />
                </Grid>
                <Grid item xs={10}>
                  <TextField
                    label="* Email"
                    id="email"
                    className={classes.input}
                    value={this.state.email}
                    onChange={this.updateEmail}
                    fullWidth={true}
                    margin="dense"
                    type='email'
                  />
                </Grid>
              </Grid>
            </GridItem>
            <GridItem xs={10} className={classes.gridItem}>
              {pass}
            </GridItem>
            <GridItem xs={10}>
              <Button variant='contained' onClick={() => {this.login()}} className={classes.loginButton}>Login</Button>
            </GridItem>
          </GridContainer>
        </Card>
      </div>
    );
  }
}

const styles = {
  main: {
    textAlign: 'center',
  },
  background: {
    backgroundImage: 'url(' + BG + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
    zIndex: -2,
    width: '1920px',
    height: '1080px',
    position: 'absolute',
    top: 0,
    left: 0
  },
  card: {
    position: 'absolute',
    left: '1190px',
    top: '200px',
    width: '500px',
    height: '540px',
    borderRadius: '4px',
    textAlign: 'center'
  },
  header: {
    marginTop: '60px',
    width: '100%',
    height: '40px',
    textAlign: 'center'
  },
  header2: {
    marginTop: '40px',
    marginBottom: '60px',
    marginLeft: '28%',
    width: '44%',
    height: '40px',
    textAlign: 'center'
  },
  loginButton: {
    marginTop: '60px',
    marginBottom: '40px',
    backgroundColor: '#012d5a',
    color: '#fff',
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));
