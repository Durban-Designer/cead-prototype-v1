export function updateUser(data) {
  return {
    type: "updateUser",
    payload: data
  };
}

export function logout() {
  return {
    type: "logout"
  };
}
