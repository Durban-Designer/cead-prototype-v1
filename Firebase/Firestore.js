import firebase from ‘firebase’;

var config = {
  apiKey: "AIzaSyBSj076LRheHKKVkpsyJVyHaTGtDNYckbM",
  authDomain: "cead-prototype.firebaseapp.com",
  databaseURL: "https://cead-prototype.firebaseio.com",
  projectId: "cead-prototype",
  storageBucket: "cead-prototype.appspot.com",
  messagingSenderId: "762506985610"
};
firebase.initializeApp(config);

export default firestore;
