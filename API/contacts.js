var express = require("express");
var mongodb = require("mongodb");
var _ = require("lodash");
var bodyParser = require("body-parser");
var passport = require("passport");
var passportJWT = require("passport-jwt");
var jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
var mongoose = require("mongoose");
var Contact = mongoose.model("Contact");

app.use(bodyParser.json());


router.post("/", (req,res) => {
  var newContact = new Contact({
    name: req.body.name,
    email: req.body.email,
    category: req.body.category,
    priority: req.body.priority,
    message: req.body.message
  })

  newContact.save((err, result) => {
    if(err) {
      res.send(err);
    } else {
      res.send('success');
    }
  })
})

module.exports = router;
